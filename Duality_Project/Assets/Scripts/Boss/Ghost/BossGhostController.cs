﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossGhostController : MonoBehaviour {

	// variable lié au déplacement
	public float speed;

	private float moveInput;
	private Vector2 globalMoveInput;
	private Vector2 moveVelocity;

	private Rigidbody2D rb;
	
	private bool facingRight = true;
	
	//variable lié à l'animation
	public Animator animator;
	
	void Start () {
		rb = GetComponent<Rigidbody2D>();
	}

	void Update()
	{
		rb.MovePosition(rb.position + moveVelocity * Time.fixedDeltaTime);
	}
	
	void FixedUpdate()
	{

		// déplacement verticale
		moveInput = Input.GetAxisRaw("BossHorizontal");
		
		// déplacement verticale + horizontale
		globalMoveInput = new Vector2(moveInput, Input.GetAxisRaw("BossVertical"));
		moveVelocity = globalMoveInput.normalized * speed;
		
		// animation
		//animator.SetFloat("Speed", Mathf.Abs(moveInput));
		
		// direction du boss 
		if (facingRight == false && moveInput > 0)
		{
			Flip();
		}
		else if (facingRight == true && moveInput < 0)
		{
			Flip();
		}
		
	}
	
	// Update is called once per frame
	
	void Flip()
	{
		facingRight = !facingRight;
		Vector3 Scaler = transform.localScale;
		Scaler.x *= -1;
		transform.localScale = Scaler;
	}
	
}
