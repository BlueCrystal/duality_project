﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {
	
	public int health;
	public Animator animator;
	private float timeBeforeDestroy = 1;

	public GameObject bloodEffect;
	
	// variable lié à la mort
	public BoxCollider2D boxColliderBeforeSupr;
	public Rigidbody2D changeGravity;

	private void Start()
	{
		// box colider enemy
		//boxColliderBeforeSupr = gameObject.GetComponent<BoxCollider>();
	}

	private void FixedUpdate()
	{
		
		// mort de l'enemie 
        if (health <= 0)
		{
            //animator.SetBool("isDead", true);
            animator.SetBool("isDead", true);
			
			// suppression de son boxcollider et mise a 0 de sa gravité 
			Destroy(boxColliderBeforeSupr);
			changeGravity.gravityScale = 0;

            if (timeBeforeDestroy > 0)
			{
				//Debug.Log(timeBeforeDestroy);
				timeBeforeDestroy -= Time.deltaTime;
			}
			else
			{
                ScoreScript.ScoreJoueur += 1;
                Destroy(gameObject);
            }
		}
    }

	public void TakeDammage(int damage)
	{
		// son de dommage ( non crée )   
		// effet de particules de sang ( non crée ) 
		//Instantiate(bloodEffect, transform.position, Quaternion.identity);
		health -= damage;
		//Debug.Log("damage taken");
	}
    
}
