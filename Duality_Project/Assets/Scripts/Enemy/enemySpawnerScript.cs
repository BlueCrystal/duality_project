﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemySpawnerScript : MonoBehaviour
{

	public GameObject enemy;
	public GameObject enemy2;
	private Vector2 whereToSpawn;
	public float nextSpawn;
	private float resultSpawnRandomTime;
	
	// choix aléatoire des enemy
	private float choixEnemy;
	
	void Start () {
		SpawnRandomTime();
		functionChoixEnemy();
	}

	void functionChoixEnemy()
	{
		choixEnemy = Random.Range(0, 101);
	}
	
	void SpawnRandomTime()
	{
		resultSpawnRandomTime = Random.Range(3, nextSpawn);
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (resultSpawnRandomTime <= 0)
		{
			whereToSpawn = new Vector2(transform.position.x, transform.position.y);

			if (choixEnemy > 20)
			{
				Instantiate(enemy, whereToSpawn, Quaternion.identity);
				functionChoixEnemy();
			}
			else if (choixEnemy <= 20)
			{
				Instantiate(enemy2, whereToSpawn, Quaternion.identity);
				functionChoixEnemy();
			}
			SpawnRandomTime();
		}
		else
		{
			resultSpawnRandomTime -= Time.deltaTime;
		}
	}
}
