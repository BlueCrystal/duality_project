﻿	using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.UIElements;

public class skeletonWizardPatrol : MonoBehaviour {

	//variable lié au déplacement
	public Transform groundDetection;
	public Transform wallDetection;
	public LayerMask whatIsGround;

	public Transform playerDetection;
	public LayerMask whatIsPlayer;
	private Vector2 alignement;
		
	private Transform Enemy;
	
	public float distanceGround;
	public float distanceWall;
	public float distancePlayer;
	
	private bool movingRight;
	public float speed;
	private float directionRadom;
	private int directionSpawn;
	
	// variable lié a l'animation
	public Animator animator;
	
	// variable lié aux comportement
	private float TimeBtwComportments;
	public float StartTimeBtwComportments;
	private float randomTime;
	
	// variable lié à l'attque
	public float attackRangeX;
	public float attackRangeY;
	public Transform attackPos_enemy;

	private float timeBeforeAttack;
	public float startTimeBeforeAttack;
	private float timeBtwAttack;
	public float startTimeBtwAttack;
	private float timeBeforeEndAttack;

	private bool wantShoot;
	public Transform FirePoint;
	public GameObject flameShootPrefab;
	
	public int damage;
	
	void Start () {
		
		animator.SetBool("isIdle", false);
		animator.SetBool("isPatrol", true);
		
		// temps aléatoire entre les comportements
		TimeBtwComportements();
		timeBtwAttack = 0;
		timeBeforeAttack = startTimeBeforeAttack;
		
		// initialisation des variables lié à l'attaque
		timeBeforeEndAttack = 1.05f;
		wantShoot = false;
		
		// direction aléatoire au spawn
		directionRadom = Random.Range(0, 2);
		if (directionRadom >=0 & directionRadom < 1)
		{
			directionSpawn = 1;
			//Debug.Log("droite");
		}
		else if (directionRadom >= 1)
		{
			directionSpawn = 0;
			//Debug.Log("gauche");
		}
		
		if (directionSpawn == 0)
		{
			gameObject.transform.eulerAngles = new Vector3(0, -180, 0);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
		// detection du sol
		RaycastHit2D groundInfo = Physics2D.Raycast(groundDetection.transform.position, Vector2.down, distanceGround);

		// detection des murs
		RaycastHit2D wallInfo = Physics2D.Raycast(wallDetection.transform.position, Vector2.up, distanceWall, whatIsGround);
		
		// detection du joueur
		if (movingRight)
		{
			alignement = Vector2.right;
		}
		else
		{
			alignement = Vector2.left;
		}
		RaycastHit2D playerInfo = Physics2D.Raycast(playerDetection.transform.position, alignement, distancePlayer, whatIsPlayer);

		// changement de comportement
		if (TimeBtwComportments <= 0)
		{
			if (animator.GetBool("isPatrol") == true & animator.GetBool("isIdle") == false)
			{
				animator.SetBool("isPatrol", false);
				animator.SetBool("isIdle", true);
				TimeBtwComportements();
			}
			else if (animator.GetBool("isIdle") == true & animator.GetBool("isPatrol") == false)
			{
				animator.SetBool("isPatrol", true);
				animator.SetBool("isIdle", false);
				TimeBtwComportements();
			}
		}
		else
		{
			//Debug.Log(TimeBtwComportments);
			TimeBtwComportments -= Time.deltaTime;
		}
		
		// comportement d'attaque
		if (playerInfo.collider == true)
		{
			wantShoot = true;
		}
		
		if  (wantShoot == true & timeBtwAttack <= 0)
		{
			//Debug.Log("personnage repéré !");
			animator.SetBool("isIdle", false);
			animator.SetBool("isPatrol", false);
			animator.SetBool("isAttack", true);
		
			//Debug.Log(timeBeforeAttack);
			if (timeBeforeAttack <= 0)
			{	
			Shoot();
			timeBtwAttack = startTimeBtwAttack;
			timeBeforeAttack = startTimeBeforeAttack;
			wantShoot = false;
			}
			else
			{
				timeBeforeAttack -= Time.deltaTime;
			}
		}
		else
		{
			timeBtwAttack -= Time.deltaTime;
			timeBeforeAttack = startTimeBeforeAttack;
		}

		if (Input.GetKeyDown(KeyCode.G))
		{
			Shoot();
		}
		
		// temps avant la fin de l'animation d'attaque
		if (animator.GetBool("isAttack") == true)
		{
			//Debug.Log(timeBeforeAttack);
			//Debug.Log(timeBeforeEndAttack);
			if (timeBeforeEndAttack <= 0)
			{
				animator.SetBool("isAttack", false);
				animator.SetBool("isPatrol", true);
				timeBeforeEndAttack = 1.05f;
			}
			else
			{
				timeBeforeEndAttack -= Time.deltaTime;
			}
		}
		
		// comportement de patrouille
		if (animator.GetBool("isDead") == false & animator.GetBool("isIdle") == false & animator.GetBool("isPatrol") == true)
		{	
			gameObject.transform.Translate(Vector2.right * speed * Time.deltaTime);
		
			if (groundInfo.collider == false | wallInfo.collider == true )
			{
				if (movingRight)
				{
					//Debug.Log("droite");
					gameObject.transform.eulerAngles = new Vector3(0, -180, 0);
					movingRight = false;
				}
				else
				{
					//Debug.Log("gauche");
					gameObject.transform.eulerAngles = new Vector3(0, 0, 0);
					movingRight = true;
				}
			}
		}
		/*else if((animator.GetBool("isDead") == false & animator.GetBool("isIdle") == true & animator.GetBool("isPatrol") == false)
		{
			
		}
		*/
	}

	void TimeBtwComportements()
	{
		TimeBtwComportments = Random.Range(0, StartTimeBtwComportments);
	}

	void Shoot()
	{
		Instantiate(flameShootPrefab, FirePoint.position, FirePoint.rotation);
	}
	
	void dammagePlayer()
	{
        GameObject.Find("Player").GetComponent<PlayerController>().TakeDammage(damage);
	}
}
