﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class flameShoot : MonoBehaviour
{

	public Transform playerDetection;
	public float distancePlayer;
	public LayerMask whatIsPlayer;
	
	public float speed;
	public Rigidbody2D rb;
	
	// Use this for initialization
	void Start ()
	{		
		rb.velocity = transform.right * speed;
	}

	private void Update()
	{
		RaycastHit2D playerInfo = Physics2D.Raycast(playerDetection.transform.position, Vector2.right, distancePlayer, whatIsPlayer);

		if (playerInfo.collider)
		{
			Destroy(gameObject);
		}
	}
	// Update is called once per frame
	
}
