﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.IO;
using TMPro;

public class PlayerController : MonoBehaviour
{

	// variable lié à la santé du joueur 
	public int health;
	private float TimeInvincibility;

	public float StartTimeInvincibility;

    public TextMeshProUGUI affHealth;

    // variable lié au déplacement
    public float speed;
	public float jumpForce;
	private float moveInput;

	private Rigidbody2D rb;

	private bool facingRight = true;

	private bool isGrounded;
	public Transform groundCheck;
	public float checkRadius;
	public LayerMask whatIsGround;

	//variable lié à l'animation
	public Animator animator;

	// variable lié au double / triple saut
	private int extraJumps;
	public int extraJumpsValue;

	// position du joueur
	public GameObject objectPlayer;
	private Vector3 position;
	private Vector3 positionNext;
		
	//récupération du score 
	public string getHighScore;
	private int parseHighScore;
		
	// opacité du personnage 
	public SpriteRenderer playerSprite;
	
	void Start()
	{
		rb = GetComponent<Rigidbody2D>();

        // double saut
        extraJumps = extraJumpsValue;

			
	}

	void FixedUpdate()
	{
		// animation
		animator.SetFloat("speed", Mathf.Abs(moveInput));

		// vérifie si le joueur touche le sol 
		isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, whatIsGround);

		// position
		position = GameObject.Find("Player").transform.position;

		// déplacement horizontal 
		moveInput = Input.GetAxisRaw("Horizontal") * speed;

		rb.velocity = new Vector2(moveInput, rb.velocity.y);

		// direction du personnage 
		if (facingRight == false && moveInput > 0)
		{
			Flip();
		}
		else if (facingRight == true && moveInput < 0)
		{
			Flip();
		}
	}

	private void Update()
	{
		// FONCTION DE SAUT 
		if (isGrounded == true)
		{
			extraJumps = extraJumpsValue;
		}

		//position next
		positionNext = GameObject.Find("Player").transform.position;

		// saut suplémentaire
		if (Input.GetButtonDown("Jump") && extraJumps > 0)
		{
			rb.velocity = Vector2.up * jumpForce;
			extraJumps--;
			//animator.SetBool("isJumping", true);
		}
		else if (Input.GetButtonDown("Jump") && extraJumps == 0 && isGrounded)
		{
			rb.velocity = Vector2.up * jumpForce;
			//animator.SetBool("isJumping", true);
		}

		// animation de saut
		if (Input.GetButton("Jump") & !isGrounded)
		{
			// si le joueur monte : animation jump 
			if (position.y < positionNext.y)
			{
				animator.SetBool("isJumping", true);
				animator.SetBool("isFalling", false);
			}
			// si le joueur descend : animation chute 
			else
			{
				animator.SetBool("isJumping", false);
				animator.SetBool("isFalling", true);
			}
		}
		// si le joueur touche le sol : arret animation
		else if (isGrounded)
		{
			animator.SetBool("isJumping", false);
			animator.SetBool("isFalling", false);
		}

        // affichage de la santé 
        affHealth.text = "x" + health.ToString();

		if (TimeInvincibility > 0)
		{
			// opacité du personnage 
			playerSprite.color = new Color(1f, 1f, 1f, .5f);

		}
		else
		{
			playerSprite.color = new Color(1f, 1f, 1f, 1f);

		}
		
		TimeInvincibility -= Time.deltaTime;
		
		// si le joueur meurt 
		if (health <= 0)
		{
            // Récupère le Highscore actuel
			getHighScore = PlayerPrefs.GetString("score");

            parseHighScore = Int32.Parse(getHighScore);

            // Si le highscore actuel est inférieur au score actuel, alors on remplace l'ancien Highscore par le nouveau
            if (parseHighScore < ScoreScript.ScoreJoueur)
            {
                Debug.Log(parseHighScore);
                Debug.Log(ScoreScript.ScoreJoueur);
                PlayerPrefs.SetString("score", ScoreScript.ScoreJoueur.ToString());
            }

            // On "tue" le joueur, en détruisant son objet
            Destroy(gameObject);

			// Retour au menu
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);

            // Reset du score
            ScoreScript.ScoreJoueur = 0;
		}
		
	}

	private void OnCollisionEnter2D(Collision2D col)
	{ 
		if (col.gameObject.tag.Equals("Skeleton"))
		{
			if (TimeInvincibility <= 0)
			{
				Debug.Log("colision");
				health = health - 1;
				TimeInvincibility = StartTimeInvincibility;
			}
			
		}
		//Debug.Log(TimeInvincibility); 
	}

	void Flip()
	{
		facingRight = !facingRight;
		Vector3 Scaler = transform.localScale;
		Scaler.x *= -1;
		transform.localScale = Scaler;
	}

	public void TakeDammage(int damage)
	{
		
		// son de dommage ( non créé )   
		// effet de particules de sang ( non créé ) 
		//Instantiate(bloodEffect, transform.position, Quaternion.identity);
		if (TimeInvincibility <= 0)
		{
			
			health -= damage;
			Debug.Log("Damages taken!");
			TimeInvincibility = StartTimeInvincibility;
		}
	}
}
