﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    private float TimeBtwAttack;
    public float StartTimeBtwAttack;

    public Transform attackPos;
    public LayerMask whatIsEnemies;
    
    // Cercle de detection
    public float attackRange;
    
    // Rectangle de detection
    public float attackRangeX;
    public float attackRangeY;
    
    public int damage;
    
    private Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (TimeBtwAttack <= 0)
        {
            if (Input.GetButtonDown("Attack"))
            {

                //animation de vibration (non crée)
                //camAnim.SetTrigger("shake");
                
                //animation d'attaque
                //PlayerAnim.SetTrigger("attack");
                
                // cercle de detection d'enemies
                //Collider2D[] enemiesToDamage = Physics2D.OverlapCircleAll(attackPos.position, attackRadius, whatIsEnemies);
                
                // rectangle de detection d'enemies
                Collider2D[] enemiesToDamage = Physics2D.OverlapBoxAll(attackPos.position, new Vector2(attackRangeX, attackRangeY), 0, whatIsEnemies);
                
                
                for (int i = 0; i < enemiesToDamage.Length; i++)
                {
                    enemiesToDamage[i].GetComponent<Enemy>().TakeDammage(damage);
                }
                // animation 
                animator.SetTrigger("isAttacking");
                TimeBtwAttack = StartTimeBtwAttack;
            }
        }
        else
        {
            TimeBtwAttack -= Time.deltaTime;
        }
    }

    private void OnDrawGizmoSelected()
    {
        Gizmos.color = Color.red;
        // attaque cercle 
        //Gizmos.DrawWireSphere(attackPos.position, attackRange);
        
        //attaque rectangle 
        Gizmos.DrawWireCube(attackPos.position, new Vector3(attackRangeX, attackRangeY, 1));
    }
}
 