﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScoreScript : MonoBehaviour
{
    
    // affichage de high score 
    public TextMeshProUGUI affHighscore;
    public string getHighScore;
    
    public static int ScoreJoueur = 0;
    private Text Score;

    // scene actuelle 
    private Scene m_Scene;
    private string sceneName;
    
    void Start()
    {
        // affichage du high score en jeux 
        Score = GetComponent<Text>(); 

        // affichage du meilleur high score au menu 
        getHighScore = PlayerPrefs.GetString("score");
    }
    
    // Update is called once per frame
    void Update()
    {
        m_Scene = SceneManager.GetActiveScene();
        sceneName = m_Scene.name;
        
        if (sceneName == "Game")
        {
            Score.text = "Score: " + ScoreJoueur;
        }
        else if (sceneName == "Menu")
        {
            affHighscore.text = "highScore: " + getHighScore;
        }
    }
}
